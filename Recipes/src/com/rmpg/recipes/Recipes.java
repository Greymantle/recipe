package com.rmpg.recipes;

public class Recipes
{ 
	RecipeCard[] cookBook;
	
	public Recipes()
	{
	    
	}
	    public static void main(String[] args)
	{
	    Recipes program = new Recipes();
	    program.run();	
	}
	
	public void run()
	{
	    RecipeCard[] cookbook = createCookBook();
        cookbook[sortRecipesByCostPerServing(cookbook)].print();
	}
	
	public RecipeCard[] createCookBook()
	{
		RecipeCard[] cookBook = new RecipeCard[0];
		RecipeCard thisRecipe = new RecipeCard();
		String ingListStr = IngredientString.getString();
		while (ingListStr.indexOf(";") != -1);
		{
			String nextRecipe = ingListStr.substring(0, ingListStr.indexOf(";"));
			ingListStr = ingListStr.substring(ingListStr.indexOf(";"));
			increaseCookbookSize(cookBook);
			cookBook[cookBook.length - 1] = thisRecipe.fillOutRecipeCard(nextRecipe);;
		}
		return cookBook;
	}

    private  RecipeCard[] increaseCookbookSize(RecipeCard[] cookBookToBeAddedTo)
	{
		RecipeCard[] tempArray = new RecipeCard[cookBookToBeAddedTo.length + 1];
		for (int index = 0; index < cookBookToBeAddedTo.length; index++)
			tempArray[index] = cookBookToBeAddedTo[index];
		cookBookToBeAddedTo = tempArray;
		return cookBookToBeAddedTo;
	}

	private static RecipeCard[] addToCookBook(RecipeCard[] cookBookToBeAddedTo, String someRecipe)
	{
		RecipeCard[] tempArray = new RecipeCard[cookBookToBeAddedTo.length + 1];
		for (int index = 0; index < cookBookToBeAddedTo.length; index++)
			tempArray[index] = cookBookToBeAddedTo[index];
		cookBookToBeAddedTo = tempArray;
		RecipeCard any = new RecipeCard();
		cookBookToBeAddedTo[tempArray.length - 1] = any.fillOutRecipeCard(someRecipe);
		return cookBookToBeAddedTo;
	}	


	private  int sortRecipesByCostPerServing(RecipeCard[] unsortedRecipeCards)
	{
		int numberOfCard = 0;
		for (numberOfCard = 0; numberOfCard < unsortedRecipeCards.length - 1; numberOfCard++)
		{
			for (int nextCard = numberOfCard + 1; nextCard < unsortedRecipeCards.length - 3; nextCard++)
			{
				if (unsortedRecipeCards[nextCard] == null) return numberOfCard;
				else if ((unsortedRecipeCards[nextCard].getCost() / unsortedRecipeCards[nextCard].getNumOfServings()) <
						(unsortedRecipeCards[numberOfCard].getCost() / unsortedRecipeCards[numberOfCard].getNumOfServings()))
						numberOfCard = nextCard;
			}
		}
		return numberOfCard;
	}

}
