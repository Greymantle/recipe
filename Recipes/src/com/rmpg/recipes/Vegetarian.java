package com.rmpg.recipes;

public enum Vegetarian
{
	VEGAN,
	OVOVEGETARIAN,
	PESCATARIAN,
	OMNIVORE,
	LACTOVEGETARIAN,
	LACTOOVOVEGETARIAN;
}
