package com.rmpg.recipes;

public enum measureUnits 
{
	GRAMS,
	OUNCES,
	TEASPOON,
	TABLESPOON,
	DRY_CUP,
	LIQUID_CUP;
}
